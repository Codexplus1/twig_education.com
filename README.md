# Twig Education Test

This test is to get result of groupedElements based on N (positive integer ) value passed as second argument to function
"groupdArrayElements(arg1, arg2)".

#Arguments 
arg1 = arrayOfElements
arg2 = numberOfGroups

# Implementation

function = groupArrayElements(arrayOfElements, numberOfGroups)

1. Compute the number of occurrences to a whole number.
2. Check if second argument is greater than the total length of the given array, return empty array;
   "This is to ensure that this function will always return the same data type".
3. Iterate the array size using While Loop and increment the value of "i" (which is the loop counter) by "n" number (that represents the number of elements that should be in each group).
4. While in the loop, slice out "n" number of array and store as group in groupElements array;


# Why it works

This worked because the "n" number of occurrences were first computed to a whole number to determie the number of elements that should be in each group.
Also incrementing the counter by "n" number of occurrences during iteration helps to slice out n number of elements that should be in each group.